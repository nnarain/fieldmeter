#ifndef DISPLAY
#define DISPLAY

#include <nnarain/boards/atmega328p.h>

#define DELAY 1/24

typedef struct{
	uint8_t* displayPins;
	uint8_t exponentPin;
	uint8_t* galLogicPins;
	int numDisplays;
}DISPLAY_MODULE;

void convertBase(int num, int base, int arr[])
{

	while(num > 0){	
		*(arr++) = (num % base);
		num /= base;
	}

/*	int index = 0;
	while(num > 0){
		arr[index++] = (num % base);
		num /= base;
	}
*/
}

void writeToDisplay(DISPLAY_MODULE display, int num, int exponent)
{
	int base10[3] = {0,0,0};
	convertBase(num, 10, base10);

	int numbers[4] = {base10[0], base10[1], base10[2], (exponent < 0) ? exponent * (-1) : exponent};
	
	int i;
	for(i = 0; i < display.numDisplays; i++){
	
		digitalOutput(display.displayPins[i], HIGH);
		
		// exponent sign
		if(i == display.numDisplays-1){
			digitalOutput(display.exponentPin, (exponent > 0) ? LOW : HIGH);
		}
		
		int base2[4] = {0,0,0,0};
		convertBase(numbers[i], 2, base2);
		
		int j;
		for(j = 0; j < 4; j++){
		
			digitalOutput(display.galLogicPins[j], base2[j]);
		
		}
		
		_delay_ms(5);
		
		digitalOutput(display.displayPins[i], LOW);
	
	}
	
}

#endif