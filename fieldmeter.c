/**************************************
	Purpose: Fieldmeter microcontroller code
	Author: Natesh Narain
	Date: October 18, 2013
**************************************/

#include <nnarain/boards/atmega328p.h>
#include <nnarain/utils/vector.h>

#include "display.h"

#define DELAY 1/24
#define NUM_DISPLAYS 4

#define VREF 5.0f
#define SENS 2.5f/1000
#define ZERO_GAUSS 512
#define RESOLUTION (VREF/1023)
#define GAUSS_PER_STEP RESOLUTION * (1/(SENS))

#define CHAR_ZERO 48
#define DELIM ';'
#define BUF_SIZE 20

#define POSITIVE '0'
#define NEGATIVE '-'

float calculateGauss(int sensorInput);

void displayWrite(DISPLAY_MODULE, int, int);
int get3Digits(float, int*);
void clearArray(int*, int);
void transmitFieldData(int*, int);

int main(void)
{
	// Port init
	DDRB = OUTPUT;
	DDRD = OUTPUT;
	
	PORTD = 0x00;
	
	// Display init
	uint8_t GAL_LOGIC_PINS[4] = {11, 10, 9, 8};
	uint8_t display_pins[4] = {5, 6, 7, 4}; // last index exponent pin
	
	DISPLAY_MODULE display;
	display.galLogicPins = GAL_LOGIC_PINS;
	display.displayPins = display_pins;
	display.numDisplays = 4;
	display.exponentPin = 12;
	
	//
	VECTOR3F field;
	int exponent;
	
	// serial init
	initSerial();
	// analog init
	initADC();
	
	unsigned char transmitData[BUF_SIZE]; 
	// this variable is only used to read from UDR0
	unsigned char dummy;
	
	while(1){

		exponent = 0;

		int i;
		for(i = 0; i < VEC3_LEN; i++){
		
			int sensorInput = analogInput(i);
			field[i] = calculateGauss(sensorInput);
		
		}
	
		float fieldMag = mag(field);
		
		int magDec = get3Digits(fieldMag, &exponent);
	
		writeToDisplay(display, magDec, exponent);
		
		if(isHigh(UCSR0A, _BV(RXC0))){
			
			dummy = UDR0; // clear register by reading
			
			int fieldInfo[4] = {
				(int)fieldMag, (int)field[X],
				(int)field[Y], (int)field[Z]
			};
			
			int base10[3] = {0,0,0};
			int i, dataIndex = 0;
			for(i = 0; i < 4; i++){
				char sign = (fieldInfo[i] >= 0) ? POSITIVE : NEGATIVE;
				convertBase(fieldInfo[i], 10, base10);
				transmitData[dataIndex++] = sign;
				transmitData[dataIndex++] = base10[2]+CHAR_ZERO;
				transmitData[dataIndex++] = base10[1]+CHAR_ZERO;
				transmitData[dataIndex++] = base10[0]+CHAR_ZERO;
				transmitData[dataIndex++] = DELIM;
				clearArray(base10, 3);
			}
			
			serialWrite(transmitData, BUF_SIZE);
			
		}
		
	}
	
	return 1;
}

void clearArray(int arr[], int len){
	int i;
	for(i = 0; i < len; i++){
		arr[i] = 0;
	}
}

float calculateGauss(int sensorInput){

	int diff = sensorInput - ZERO_GAUSS;
	float gauss = diff * GAUSS_PER_STEP;

	return gauss;
}

int get3Digits(float value, int* exponent)
{
	value = (value < 0) ? (-1 * value) : value;
	
	if(value < 0.999999f){
	
		while(value < 0.999999f){
			value *= 10;
			*exponent -= 1;
		}

		value *= 100;
		*exponent -= 2;
	
	}else if(value > 999.0f){
	
		while(value > 999.0f){
			value /= 10;
			*exponent += 1;
		}
	
	}

	return (int) value;
}