#!C:\Anaconda\python
"""
	Purpose: Receive information from fieldmeter, graph and export to xml
	Author: Natesh Narain
"""

# imports
import sys
import time
import os
from xml.etree import ElementTree as et
from datetime import datetime

# serial communication library
import serial

# graphing library
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

# GUI library
from PySide.QtCore import *
from PySide.QtGui import *
from PySide.QtUiTools import QUiLoader

# function declaration

def loadForm(filename):
	loader = QUiLoader()
	formFile = QFile(filename)
	formFile.open(QFile.ReadOnly)
	form = loader.load(formFile, None)
	formFile.close()
	return form

def connectFunction(widget, functionName, func):
	widgetFunc = getattr(widget, functionName)
	widgetFunc.connect(func)

def parseFieldData():
	print 'Parsing field Data'
	lines = window.etSamples.toPlainText().split('\n')
	
	coords = []
	vecs = []
	mags = []
	greatestMag = 0
	
	for line in lines:
		print line
		if line == '':
			continue
		
		fieldInfo = line.split(';')
		
		x, y, z = fieldInfo[0].split(',')
		coords.append((float(x), float(y), float(z)))
		
		mag = float(fieldInfo[1])
		if mag > greatestMag:
			greatestMag = mag
		mags.append(mag)
		
		vec = float(fieldInfo[2]), float(fieldInfo[3]), float(fieldInfo[4])
		vecs.append(vec)
		
	return coords, vecs, mags, greatestMag
		

def graphPlanesMag():
	print 'Graphing Mag'
	
	fig = plt.figure()
	ax = Axes3D(fig)
	
	coords, vecs, mags, greatestMag = parseFieldData()
	
	xy = window.rbXY.isChecked()
	xz = window.rbXZ.isChecked()
	yz = window.rbYZ.isChecked()
	
	if xy:
		ax.set_xlabel('X', fontsize=14)
		ax.set_ylabel('Y', fontsize=14)
		ax.set_zlabel('B', fontsize=14)
	elif xz:
		ax.set_xlabel('X', fontsize=14)
		ax.set_ylabel('Z', fontsize=14)
		ax.set_zlabel('B', fontsize=14)
	elif yz:
		ax.set_xlabel('Y', fontsize=14)
		ax.set_ylabel('Z', fontsize=14)
		ax.set_zlabel('B', fontsize=14)
	
	size = len(mags)
	print 'Graphing %d points' % (size)
	
	for i in range(size):
		x, y, z = coords[i]
		
		if xy:
			ax.scatter(x, y, mags[i])
		elif xz:
			ax.scatter(x, z, mags[i])
		elif yz:
			ax.scatter(y, z, mags[i])
	
	plt.show()
		
def graphIntensityMap():
	print 'Graphing field intensity'
	fig = plt.figure()
	ax = Axes3D(fig)
	ax.set_xlabel('X', fontsize=14)
	ax.set_ylabel('Y', fontsize=14)
	ax.set_zlabel('Z', fontsize=14)
	
	cm = plt.get_cmap('hot')
	
	coords, vecs, mags, greatestMag = parseFieldData()
	
	for i in range(len(mags)):
		x, y, z = coords[i]
		ax.scatter(x, y, z, c=cm(mags[i]/greatestMag))
	
	plt.show()

def graphVectorMap():
	print 'Graphing vector field'
	
	fig = plt.figure()
	ax = Axes3D(fig)
	ax.set_xlabel('X', fontsize=14)
	ax.set_ylabel('Y', fontsize=14)
	ax.set_zlabel('Z', fontsize=14)
	
	coords, vecs, mags, greatestMag = parseFieldData()
	
	for i in range(len(mags)):
		x, y, z = coords[i]
		vx, vy, vz = vecs[i]
		vx /= mags[i]
		vy /= mags[i]
		vz /= mags[i]
		xe = x + vx
		ye = y + vy
		ze = z + vz
		ax.scatter(x,y,z, 'bs')
		ax.plot([x, xe], [y, ye], zs=[z, ze])
	
	plt.show()
	
def sample():
	try:
		# tell uP to transmit field info
		ser.write([1]) # [] @ conestoga, none at home...
		# wait a bit for info
		time.sleep(.1)
		# read the field info from serial port
		data = ser.readline()
		print data
		
		coord = window.leX.text()+','+window.leY.text()+','+window.leZ.text()
		# write to the display window
		fieldInfo = coord + ';' + data
		window.etSamples.append(fieldInfo+'\n')
		# write to the log file
		logFile.write(fieldInfo + '\n')
		
	except serial.SerialException:
		print 'error while reading'
		pass

def getDate():
	today = datetime.today()
	date = str(today.month)
	date += '-' + str(today.day)
	date += '-' + str(today.year)
	return date

def createFile(name, ex):
	i = 0
	filename = name + getDate() + '_'
	
	while os.path.exists(filename + str(i) + ex):
		i += 1
		
	logFile = open(filename + str(i) + ex, 'w+')
	return logFile
	
	
def exportXML():
	print 'Exporting data to Microsoft Excel XML Format...'
	print 'Creating XML Document...'
	workBook = et.Element('WorkBook')
	workBook.set('xmlns', 'urn:schemas-microsoft-com:office:spreadsheet')
	workBook.set('xmlns:ss', 'urn:schemas-microsoft-com:office:spreadsheet')
	workBook.set('xmlns:o', 'urn:schemas-microsoft-com:office:office')
	workBook.set('xmlns:x', 'urn:schemas-microsoft-com:office:excel')
	
	docProperties = et.SubElement(workBook, 'DocumentProperties')
	docProperties.set('xmlns', 'urn:schemas-microsoft-com:office:office')
	
	author = et.SubElement(docProperties, 'Author')
	author.text = "Natesh Narain"
	
	excelWorkBook = et.SubElement(workBook, 'ExcelWorkbook')
	excelWorkBook.set('xmlns', 'urn:schemas-microsoft-com:office:excel')
	ewbNodes = ['WindowHeight', 'WindowWidth', 'WindowTopX', 'WindowTopY']
	ewbNodeVals = ['6795', '8460', '120', '15']
	
	for i in range(0, len(ewbNodes)):
		node = et.SubElement(excelWorkBook, ewbNodes[i])
		node.text = ewbNodeVals[i]
		
	opts = et.SubElement(workBook, 'WorksheetOptions')
	opts.set('xmlns', 'urn:schemas-microsoft-com:office:excel')
	
	# styles
	styles = et.SubElement(workBook, 'Styles')
	style = et.SubElement(styles, 'Style')
	style.set('ss:ID', 'Default')
	style.set('ss:Name', 'Normal')
	
	workSheet = et.SubElement(workBook, 'Worksheet');
	workSheet.set('ss:Name', 'Magnetic field meter data')
	
	# title the data
	table = et.SubElement(workSheet, 'Table')
	title = et.SubElement(table, 'Row')
	title.set('ss:Index', '1')
	titleCell = et.SubElement(title, 'Cell')
	titleCell.set('ss:Index', '1')
	titleData = et.SubElement(titleCell, 'Data')
	titleData.set('ss:Type', 'String')
	titleData.text = 'Magnetic field data'
	
	headers = ['X', 'Y', 'Z', 'Magnitudes']
	
	x, y, z, m, gm = parseFieldData()
	
	for i in range(0, len(headers)):
		row = et.SubElement(table, 'Row')
		row.set('ss:Index', '2')
		cell = et.SubElement(row, 'Cell')
		cell.set('ss:Index', str(i+1))
		data = et.SubElement(cell, 'Data')
		data.set('ss:Type', 'String')
		data.text = headers[i]
	
	xmlFile = createFile('field_data_', '.xml')
	tree = et.ElementTree(workBook)
	print 'Saving xml file...'
	tree.write(xmlFile.name)
	
	xmlFile.close()
	
	with open(xmlFile.name, 'r+') as file:
		xmlData = file.read()
		file.seek(0)
		xmlStr = '<?xml version="1.0"?><?mso-application progid="Excel.Sheet"?>' + xmlData
		file.write(xmlStr)
		
	print 'Done.'
	
####################################################################################
# start app
####################################################################################

# get port and baudrate from command line arguments
port = sys.argv[1]
baudrate = int(sys.argv[2])

# open a serial connection
try:
	ser = serial.Serial(port, baudrate, timeout=0)
	print 'successful connection'
except serial.SerialException:
	print 'error occured while connecting'
	#sys.exit(1)

app = QApplication(sys.argv)

# create window from ui file
window = loadForm('fieldmeter.ui')

# link buttons to functions
connectFunction(window.bnSample, 'clicked', sample)
connectFunction(window.bnVectorGraph, 'clicked', graphVectorMap)
connectFunction(window.bnMagGraph, 'clicked', graphPlanesMag)
connectFunction(window.bnExport, 'clicked', exportXML)

# create a log file
logFile = createFile('field_log_', '.log')

# display window
window.show()

#start event loop
app.exec_()

print 'closing streams'
ser.close()
logFile.close()
